<?php

function uploadImage($image): string
{
    if ($image['error'] != 0) {
        throw new Exception("Det skjedde en feil ved opplastning av bilde.");
    }

    if (!getimagesize($image['tmp_name'])) {
        throw new Exception("Filen er ikke en gyldig bildefil.");
    }

    if ($image['size'] > 2 * 1024 * 1024) { // 5MB
        throw new Exception("Filstørrelsen er for stor. Den må være under 2MB.");
    }

    $idImage = uniqid();
    while (file_exists("/images/assets/" . $idImage)) {
        $idImage = uniqid();
    }

    if (!move_uploaded_file($image['tmp_name'], "./images/assets/" . $idImage . "." . pathinfo($image["name"], PATHINFO_EXTENSION))) {
        throw new Exception("Kunne ikke laste opp filen til webserveren.");
    }
    return "./images/assets/" . $idImage . "." . pathinfo($image["name"], PATHINFO_EXTENSION);
}

// By (phpuser at gmail dot com) at http://php.net/manual/en/features.file-upload.multiple.php - edited by Fredrik Bentzen
function reArrayFiles(&$file_post){

	$file_ary = array();
	$file_count = count($file_post['name']);
	$file_keys = array_keys($file_post);

	for ($i=0; $i<$file_count; $i++) {
		foreach ($file_keys as $key) {
			$file_ary[$i][$key] = $file_post[$key][$i];
		}
	}

	return $file_ary;
}

function isValidAttachments(array $attachments): bool
{
	foreach ($attachments as $attachment) {
		if ($attachment['error'] != 0) {
			throw new Exception("Det skjedde en feil ved opplastning ett eller flere vedlegg.");
		}

		if ($attachment['size'] > 10 * 1024 * 1024) { // 10MB
			throw new Exception("Filstørrelsen på en eller flere av filene er for stor. Hvert engelt vedlegg kan ikke overstige 10MB.");
		}
	}

	return true;
}

// By Lea Hayes at http://php.net/manual/en/function.in-array.php - edited by Fredrik Bentzen
function in_array_field($needle, $needle_field, $haystack, $strict = false) {
	if ($strict) {
		foreach ($haystack as $item)
			if (isset($item[$needle_field]) && $item[$needle_field] === $needle)
				return true;
	}
	else {
		foreach ($haystack as $item) {
			if (isset($item[$needle_field]) && $item[$needle_field] == $needle)
				return true;
		}

	}
	return false;
}

function showAttachment(Attachment $attachment)
{
	$mimeType = $attachment->getMimeType();
	$fileName = $attachment->getFileName();
	$blob = $attachment->getBlob();
	header("Content-type: $mimeType");
	header("Content-Disposition: filename=\"$fileName\"");
	echo $blob;
}