<?php

require_once 'helper.php';
if (isset($_SESSION['loggedIn'])) {
    if ($_SESSION['verified']) {
        header("Location: ./");
    }
} else {
    if (isset($_POST['submitLogin'])) {
	    $user = $user_db->getUser( $_POST['email']);
	    if($user == null) {
            echo $twig->render('templates/login.twig', array('tryAgain' => true));
        } else {
            if (password_verify($_POST['password'],$user->getPasswordHash())) {
                if ($user->getVerified()) {
                    //We decided against saving the User as an Object in the php session
                    //because we would have had to serialize Users class and it wouldn't
                    //pay off code and resource-wize, converting forth and back
                    $_SESSION['loggedIn']=true;
                    $_SESSION['id']= $user->getId();
                    $_SESSION['firstname'] = $user->getFirstname();
                    $_SESSION['lastname'] = $user->getLastname();
                    $_SESSION['email'] = $user->getEmail();
                    $_SESSION['verified'] = $user->getVerified();
                    $_SESSION['pathUserImage'] = $user->getPathUserImage();
                    $_SESSION['userHasBlog'] = $blog_db->getBlogByUser($_SESSION['id'])!=null;


                    header("Location: ./");
                } else {
                    echo $twig->render('templates/login.twig', array('notVerified' => true));
                }
            } else {
                echo $twig->render('templates/login.twig', array('tryAgain' => true));
            }
        }
    } else {
        echo $twig->render('templates/login.twig');
    }
}

