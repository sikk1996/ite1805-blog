<?php

require_once 'helper.php';

if (isset($_GET['keywords'])){
    $articles = (array) $article_db->showArticlesByKeyword($_GET['keywords']);
} else {
    $articles = (array) $article_db->showAll();
}


foreach ($articles as $article) {
	$articleId = $article->getId();
	$numberOfComments = $comment_db->getNumberOfComments($articleId);
	$comments = (array) $comment_db->showArticleComments($articleId);

	foreach ($comments as $comment) {
		$idComment = $comment->getId();
		$numberOfSubComments = $subComment_db->getNumberOfSubComments($idComment);
		$numberOfComments += $numberOfSubComments;
	}
	$article->setCommentCounter($numberOfComments);
}
echo $twig->render('templates/index.twig', array('session' => $_SESSION, 'articles' => $articles));