<?php

require_once 'helper.php';

session_unset();
session_destroy();

echo $twig->render('templates/logout.twig', array('session' => $_SESSION));