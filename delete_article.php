<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19.04.2018
 * Time: 20:06
 */

require_once 'helper.php';
$articleId = $_GET['id'];

if (isset($_SESSION['loggedIn'])) {
    if ($_SESSION['verified']) {
        $article = $article_db->showOne($articleId);
        $blog = $blog_db->getBlogByUser($_SESSION['id']);

        if ($article->getIdBlog() == $blog->getId()) { //Sjekker om brukeren eier bloggen
            if($article_db->delete($articleId)){
                header("Location: index.php");
            } else {
                header("Location: article.php?id=".$articleId);
            }
        } else {
            header("Location: index.php?urNotTheOwner");

        }
    } else {
        echo "you are not verified...";
    }
} else {
    header("Location: login.php");
}


