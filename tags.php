<?php

require_once 'helper.php';

if (isset($_GET['tag'])){
    $articles = (array) $article_db->showArticlesByTags($_GET['tag']);
    $tag=$tag_db->showTag($_GET['tag']);
} else{
    header("Location: index.php");
}


foreach ($articles as $article) {
    $articleId = $article->getId();
    $numberOfComments = $comment_db->getNumberOfComments($articleId);
    $comments = (array) $comment_db->showArticleComments($articleId);

    foreach ($comments as $comment) {
        $numberOfComments += $subComment_db->getNumberOfSubComments($comment->getId());
    }
    $article->setCommentCounter($numberOfComments);
}
echo $twig->render('templates/tags.twig', array('session' => $_SESSION, 'articles' => $articles,'tag'=>$tag));