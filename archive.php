<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 01.05.2018
 * Time: 11:07
 */

require_once 'helper.php';

if (isset($_GET['month']) && isset($_GET['year'])) {
    $articles = $article_db->showArticlesfromMonthYear($_GET['month'],$_GET['year']);

    foreach ($articles as $article) {
        $article->setCommentCounter($comment_db->getNumberOfComments($article->getId())); // Remember to add SubComments as well.
    }
    $archiveDate = date($_GET['year'].'-'.$_GET['month']);
    echo $twig->render('templates/archive.twig', array('session' => $_SESSION, 'articles' => $articles,'archiveDate'=>$archiveDate));
} else {
    header("Location: index.php");

}


