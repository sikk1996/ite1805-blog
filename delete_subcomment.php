<?php

require_once 'helper.php';
$commentId = $_GET['id'];

if (isset($_SESSION['loggedIn'])) {
    if ($_SESSION['verified']) {
        $subComment = $subComment_db->showSubComments($commentId);
        $comment = $comment_db->showComment($subComment->getIdComment());
        $article = $article_db->showOne($comment->getIdArticle());
        $blog = $blog_db->getBlogByUser($_SESSION['id']);

        if ($subComment->getIdUser() == $_SESSION['id'] || $article->getIdBlog() == $blog->getId()) { //Sjekker om brukeren eier kommentar eller om brukerer bloggeier
            $subComment_db->deleteSubComment($commentId);
            header("Location: article.php?id=".$article->getId());
        } else {
            header("Location: index.php?urNotTheOwner");

        }
    } else {
        echo "you are not verified...";
    }
} else {
    header("Location: login.php");
}


