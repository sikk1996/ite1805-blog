<?php

require_once 'helper.php';

if (isset($_GET['verificationKey']) && !preg_match('/[^a-zA-Z\d]/', $_GET['verificationKey'])) {
	$emailVerificationKey = strip_tags($_GET['verificationKey']);

	$user_db->setVerified($emailVerificationKey, true);
}

echo $twig->render('templates/verify.twig');