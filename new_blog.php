<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19.04.2018
 * Time: 20:06
 */

require_once 'helper.php';

if (isset($_SESSION['loggedIn'])) {
    if ($_SESSION['verified']) {
	    if (isset($_POST['submit']) && !empty($_POST['submit'])) {
		    $blogName = strip_tags($_POST['blogName']);
		    $about = strip_tags($_POST['about']);

		    $blog = Blog::setAttributes($_SESSION['id'],$blogName);
		    $blog->setAbout($about);
		    $idBlog = $blog_db->newBlog($blog);
            $_SESSION['userHasBlog'] = true;

            if (($_FILES['image']['size'] > 0)) {
                try {
                    $pathBannerImage = uploadImage($_FILES['image']);
	                $blog_db->setBlogBannerImage($idBlog, $pathBannerImage);
                } catch (Exception $e) {
                	$error = $e->getMessage();
                }
            }

		    if ($idBlog == -1 || isset($error)) { //If any error occurs
			    echo $twig->render('templates/new_blog.twig', array('session' => $_SESSION, 'message' => $e->getMessage()));
		    } else {
			    header("Location: blog.php?id=".$idBlog);
		    }

	    } else {
	        $blog = $blog_db->getBlogByUser($_SESSION['id']);
	        if ($blog ==null) {
                echo $twig->render('templates/new_blog.twig', array('session' => $_SESSION));

            } else {
                header("Location: edit_blog.php");

            }
	    }
    } else {
	    echo "you are not verified...";
    }
} else {
    header("Location: login.php");
}



