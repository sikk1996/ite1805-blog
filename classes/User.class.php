<?php
/**
 * Created by PhpStorm.
 * User: fredr
 * Date: 04/13/18
 * Time: 17:00
 */

class User
{
	private $id;
	private $firstname;
	private $lastname;
	private $email;
	private $passwordHash;
	private $verificationKey;
	private $verified;
	private $pathUserImage;
	private $timeCreated;

	function __construct(){}

	static function setAttributes($firstname, $lastname, $email, $passwordHash, $verificationKey) {
		$instance = new self();
		$instance->setFirstname($firstname);
		$instance->setLastname($lastname);
		$instance->setEmail($email);
		$instance->setPasswordHash($passwordHash);
		$instance->setVerificationKey($verificationKey);
		return $instance;
	}

	/**
	 * @return mixed
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getFirstname(): string
	{
		return $this->firstname;
	}

	/**
	 * @param mixed $firstname
	 */
	public function setFirstname($firstname): void
	{
		$this->firstname = $firstname;
	}

	/**
	 * @return mixed
	 */
	public function getLastname(): string
	{
		return $this->lastname;
	}

	/**
	 * @param mixed $lastname
	 */
	public function setLastname($lastname): void
	{
		$this->lastname = $lastname;
	}

	/**
	 * @return mixed
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email): void
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPasswordHash(): string
	{
		return $this->passwordHash;
	}

	/**
	 * @param mixed $passwordHash
	 */
	public function setPasswordHash($passwordHash): void
	{
		$this->passwordHash = $passwordHash;
	}

	/**
	 * @return mixed
	 */
	public function getVerificationKey()
	{
		return $this->verificationKey;
	}

	/**
	 * @param mixed $verificationKey
	 */
	public function setVerificationKey($verificationKey): void
	{
		$this->verificationKey = $verificationKey;
	}

	/**
	 * @return mixed
	 */
	public function getVerified()
	{
		return $this->verified;
	}

	/**
	 * @param mixed $verified
	 */
	public function setVerified($verified): void
	{
		$this->verified = $verified;
	}

	/**
	 * @return mixed
	 */
	public function getPathUserImage()
	{
		return $this->pathUserImage;
	}

	/**
	 * @param mixed $pathUserImage
	 */
	public function setPathUserImage($pathUserImage): void
	{
		$this->pathUserImage = $pathUserImage;
	}

	/**
	 * @return mixed
	 */
	public function getTimeCreated(): string
	{
		return $this->timeCreated;
	}

	/**
	 * @param mixed $timeCreated
	 */
	public function setTimeCreated($timeCreated): void
	{
		$this->timeCreated = $timeCreated;
	}


}