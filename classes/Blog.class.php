<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 17.04.2018
 * Time: 15:18
 */

class Blog
{
    private $id;
    private $idUser;
    private $blogName;
    private $about;
    private $pathBannerImage;
    private $timeCreated;

    function __construct(){}

    static function setAttributes($idUser, $blogName) {
        $instance = new self();
        $instance->setIdUser($idUser);
        $instance->setBlogName($blogName);
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getBlogName()
    {
        return $this->blogName;
    }

    /**
     * @param mixed $blogName
     */
    public function setBlogName($blogName): void
    {
        $this->blogName = $blogName;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     */
    public function setAbout($about): void
    {
        $this->about = $about;
    }

    /**
     * @return mixed
     */
    public function getPathBannerImage()
    {
        return $this->pathBannerImage;
    }

    /**
     * @param mixed $pathBannerImage
     */
    public function setPathBannerImage($pathBannerImage): void
    {
        $this->pathBannerImage = $pathBannerImage;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @param mixed $timeCreated
     */
    public function setTimeCreated($timeCreated): void
    {
        $this->timeCreated = $timeCreated;
    }
}