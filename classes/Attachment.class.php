<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 17.04.2018
 * Time: 16:10
 */

class Attachment
{
    private $id;
    private $idArticle;
    private $size;
    private $mimeType;
    private $filename;
    private $blob;

    function __construct(){}

    static function setAttributes($idArticle, $size, $mimeType, $filename, $blob) {
        $instance = new self();
        $instance->setIdArticle($idArticle);
        $instance->setSize($size);
        $instance->setMimeType($mimeType);
        $instance->setFilename($filename);
        $instance->setBlob($blob);
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * @param mixed $idArticle
     */
    public function setIdArticle($idArticle): void
    {
        $this->idArticle = $idArticle;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType): void
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getBlob()
    {
        return $this->blob;
    }

    /**
     * @param mixed $blob
     */
    public function setBlob($blob): void
    {
        $this->blob = $blob;
    }
}