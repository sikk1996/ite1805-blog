<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 17.04.2018
 * Time: 16:16
 */

class tag
{
    private $id;
    private $tag;
    private $idBlog;

    function __construct(){}

    static function setAttributes($idBlog, $tag) {
        $instance = new self();
        $instance->setIdBlog($idBlog);
        $instance->setTag($tag);
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getIdBlog()
    {
        return $this->idBlog;
    }

    /**
     * @param mixed $idBlog
     */
    public function setIdBlog($idBlog): void
    {
        $this->idBlog = $idBlog;
    }
}