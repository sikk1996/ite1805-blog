<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 19.04.2018
 * Time: 17:41
 */

class Comment_db
{
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function showArticleComments(int $idArticle): array
    {
        $comments = array();
        try
        {
            $sth = $this->db->prepare("SELECT * FROM Comments WHERE idArticle = :idArticle ORDER BY timeCreated ");
            $sth->bindParam(':idArticle', $idArticle, PDO::PARAM_INT, 11);
            $sth->execute();

            while ($comment = $sth->fetchObject('Comment')) {
                $comments[] = $comment;
            }
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $comments;
    }
    public function showComment(int $idComment): Comment
    {
        $comment = null;
        try
        {
            $sth = $this->db->prepare("SELECT * FROM Comments WHERE id = :id");
            $sth->bindParam(':id', $idComment, PDO::PARAM_INT, 11);
            $sth->execute();
            $comment = $sth->fetchObject('Comment');
            return $comment;
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }

    public function newComment(int $idArticle, int $idUser, string $comment)
    {
        try
        {
            $sth = $this->db->prepare("INSERT INTO Comments (`idArticle`, `idUser`, `comment`) VALUES (:idArticle, :idUser, :comment);");
            $sth->bindParam(':idArticle', $idArticle , PDO::PARAM_INT, 11);
            $sth->bindParam(':idUser', $idUser, PDO::PARAM_INT, 11);
            $sth->bindParam(':comment', $comment, PDO::PARAM_STR, 65536);
            $sth->execute();
        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }

    public function getNumberOfComments(int $idArticle): int
    {
        $numberOfComments = 0;
        try
        {
            $sth = $this->db->prepare("SELECT COUNT(id) FROM Comments WHERE idArticle = :idArticle");
            $sth->bindParam(':idArticle', $idArticle , PDO::PARAM_INT, 11);
            $sth->execute();
            $numberOfComments = $sth->fetchColumn();

        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $numberOfComments;
    }
    public function deleteComment(int $idComment)
    {
        try
        {
            $sth = $this->db->prepare("DELETE FROM Comments WHERE id = :id");
            $sth->bindParam(':id', $idComment, PDO::PARAM_INT, 11);
            $sth->execute();
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }
}