<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19.04.2018
 * Time: 17:31
 */

class Tag_db {

    private $db;

    public function __construct(PDO $db){
        $this->db = $db;
    }
    public function showAllTags() : array {
        $tags = array();
        try {
            $sth = $this->db->prepare("SELECT * FROM Tags");
            $sth->execute();
            while ($tag = $sth->fetchObject("Tag")) {
                $tags[] = $tag;
            }
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $tags;
    }
    public function showTagsByBlog(int $IdBlog) : array {
	    $tags = array();
        try {
            $sth = $this->db->prepare("SELECT * FROM Tags WHERE idBlog = :idBlog");
            $sth->bindParam(':idBlog', $IdBlog, PDO::PARAM_INT, 11);
            $sth->execute();
            while ($tag = $sth->fetchObject("Tag")) {
                $tags[] = $tag;
            }
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $tags;
    }
    public function showTag(int $Id) {
        $tag = null;
        try {
            $sth = $this->db->prepare("SELECT * FROM stud_v18_bentzen.Tags where id = :id");
            $sth->bindParam(':id', $Id, PDO::PARAM_INT, 11);
            $sth->execute();
            $tag = $sth->fetchObject("Tag");
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $tag;
    }

    public function showTagsByArticle(int $id) : array {
        $tags = array();
        try {
            $sth = $this->db->prepare("SELECT * FROM ArticleTags LEFT JOIN Tags ON ArticleTags.idTag = Tags.id WHERE idArticle = :id");
            $sth->bindParam(':id',$id,PDO::PARAM_INT,11);
            $sth->execute();
            while($tag = $sth->fetchObject("Tag")){
                $tags[] = $tag;
            }
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $tags;
    }

    public function add(Tag $tag) {
        $idBlog = $tag->getIdBlog();
        $tag = $tag->getTag();
        try {
            $sth = $this->db->prepare("INSERT INTO Tags (`idBlog`, `tag`) VALUES (:idBlog, :tag);");
            $sth->bindParam(':idBlog', $idBlog, PDO::PARAM_INT,11);
            $sth->bindParam(':tag', $tag, PDO::PARAM_STR, 20);
            $sth->execute();
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
    }

    public function edit(Tag $tag) {
        $id = $tag->getId();
        $idBlog = $tag->getIdBlog();
        $tag = $tag->getTag();
        try {
            $sth = $this->db->prepare("UPDATE Tags SET id = :id, idBlog = :idBlog, tag = :tag WHERE id = :id;");
            $sth->bindParam(':id', $id , PDO::PARAM_INT, 11);
            $sth->bindParam(':idBlog', $idBlog, PDO::PARAM_INT,11);
            $sth->bindParam(':tag', $tag, PDO::PARAM_STR, 20);
	        $sth->execute();
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
    }

	public function applyToArticle(int $idTag, int $idArticle) {
		try {
			$sth = $this->db->prepare("INSERT INTO ArticleTags (`idArticle`, `idTag`) VALUES (:idArticle, :idTag);");
			$sth->bindParam(':idArticle', $idArticle, PDO::PARAM_INT,11);
			$sth->bindParam(':idTag', $idTag, PDO::PARAM_INT, 11);
			$sth->execute();
		} catch (InvalidArgumentException $e) {
			print $e->getMessage() . PHP_EOL;
		}
	}

	public function clearAllTagsFromArticle(int $idArticle)
	{
		try {
			$sth = $this->db->prepare("DELETE FROM ArticleTags WHERE idArticle = :idArticle");
			$sth->bindParam(':idArticle', $idArticle, PDO::PARAM_INT,11);
			$sth->execute();
		} catch (InvalidArgumentException $e) {
			print $e->getMessage() . PHP_EOL;
		}
	}
}