<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 17.04.2018
 * Time: 17:31
 */

class Attachment_db {

    private $db;

    public function __construct(PDO $db){
        $this->db = $db;
    }

    public function showAllByArticle(int $idArticle): array {
	    $attachments = array();
        try {
            $sth = $this->db->prepare("SELECT * FROM Attachments WHERE idArticle = :idArticle");
            $sth->bindParam(':idArticle', $idArticle, PDO::PARAM_INT, 11);
            $sth->execute();
	        while ($attachment = $sth->fetchObject("Attachment")) {
		        $attachments[] = $attachment;
	        }
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $attachments;
    }

    public function add(Attachment $attachment) : String {
        $idArticle = $attachment->getIdArticle();
        $size = $attachment->getSize();
        $mimeType = $attachment->getMimeType();
        $filename = $attachment->getFilename();
        $blob = $attachment->getBlob();

        $id = "error";
        try {
            $sth = $this->db->prepare("INSERT INTO Attachments (`idArticle`, `size`, `mimeType`, `filename`, `blob`) VALUES (:idArticle, :size, :mimeType, :filename, :blob);");
            $sth->bindParam(':idArticle', $idArticle, PDO::PARAM_INT,11);
            $sth->bindParam(':size', $size, PDO::PARAM_STR, 10);
            $sth->bindParam(':mimeType', $mimeType, PDO::PARAM_STR, 50);
            $sth->bindParam(':filename', $filename, PDO::PARAM_STR, 50);
            $sth->bindParam(':blob', $blob, PDO::PARAM_STR);
            if ($sth->execute() === true) {
                $id = $this->db->lastInsertId();
            }
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $id;
    }

    public function edit(Attachment $attachment) {
        $id = $attachment->getId();
        $idArticle = $attachment->getIdArticle();
        $size = $attachment->getSize();
        $mimeType = $attachment->getMimeType();
        $filename = $attachment->getFilename();
        $blob = $attachment->getBlob();

        try {
            $sth = $this->db->prepare("UPDATE Attachments SET id = :id, idArticle = :idArticle, size = :size, mimetype = :mimeType, filename= :filename, `blob`= :blob WHERE id = :id;");
            $sth->bindParam(':id', $id , PDO::PARAM_STR, 36);
            $sth->bindParam(':idArticle', $idArticle, PDO::PARAM_INT,11);
            $sth->bindParam(':size', $size, PDO::PARAM_STR, 10);
            $sth->bindParam(':mimeType', $mimeType, PDO::PARAM_STR, 50);
            $sth->bindParam(':filename', $filename, PDO::PARAM_STR, 50);
            $sth->bindParam(':blob', $blob, PDO::PARAM_STR);
            $sth->execute();
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
    }
}
?>