<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 17.04.2018
 * Time: 17:31
 */

class Article_db {

    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function showAll(): array
    {
        $articles= array();
        try
        {
            $result = $this->db->query("SELECT * FROM Articles ORDER BY timeCreated DESC");
            while ($article = $result->fetchObject('Article')) {
                $articles[] = $article;
            }
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $articles;
    }
    public function showAlByBlog(int $idBlog)
    {
        $articles= array();
        try
        {
            $result = $this->db->prepare("SELECT * FROM Articles WHERE idBlog = :idBlog ORDER BY timeCreated DESC");
            $result->bindParam(':idBlog', $idBlog, PDO::PARAM_INT);
            $result->execute();

            while ($article = $result->fetchObject('Article')) {
                $articles[] = $article;
            }
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $articles;
    }

    public function showArticlesByTags(int $idTag) : array {
        $articles = array();
        try {
            $sth = $this->db->prepare("SELECT idArticle FROM ArticleTags WHERE idTag = :idTag");
            $sth->bindParam(':idTag',$idTag,PDO::PARAM_INT,11);
            $sth->execute();
            while($idArticles = $sth->fetchColumn()){
                $articles[] = $this->showOne($idArticles);
            }
        } catch (InvalidArgumentException $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $articles;
    }

    public function showOne(int $id) : Article
    {
        $article = null;
        $this->addVisitor($id);
        try
        {
            $sth = $this->db->prepare("SELECT * FROM Articles WHERE id = :id");
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
            $sth->execute();
            $article = $sth->fetchObject("Article");
        }
        catch (InvalidArgumentException $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $article;
    }

    public function addVisitor(int $id){
        try
        {
            $sth = $this->db->prepare("UPDATE Articles SET counter = counter + 1 WHERE id = :id");
            $sth->bindParam(':id', $id, PDO::PARAM_INT,11);
            $sth->execute();
        }
        catch (InvalidArgumentException $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }

    public function add(Article $article) : int
    {
    	$idBlog = $article->getIdBlog();
        $title = $article->getTitle();
        $content = $article->getContent();
        $idArticle = -1;
        try
        {
            $sth = $this->db->prepare("INSERT INTO Articles (`idBlog`, `title`, `content`) VALUES (:idBlog, :title, :content);");
	        $sth->bindParam(':idBlog', $idBlog, PDO::PARAM_INT, 11);
            $sth->bindParam(':title', $title , PDO::PARAM_STR, 100);
            $sth->bindParam(':content', $content, PDO::PARAM_STR);
            if ($sth->execute() === true) {
                $idArticle = $this->db->lastInsertId();
            }
        }
        catch (InvalidArgumentException $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $idArticle;
    }

    public function delete(int $idArticle) : bool
    {
        try
        {
            $sth = $this->db->prepare("DELETE FROM Articles WHERE id = :id;");
            $sth->bindParam(':id', $idArticle, PDO::PARAM_INT, 11);
            if ($sth->execute() === true) {
                return true;
            } else {
                return false;
            }
        }
        catch (InvalidArgumentException $e)
        {
            print $e->getMessage() . PHP_EOL;
            return false;
        }
    }

    public function edit(Article $article)
    {
        $id = $article->getId();
        $title = $article->getTitle();
        $content = $article->getContent();
	    $lastEdited = date("Y-m-d H:i:s");
        try
        {
            $sth = $this->db->prepare("UPDATE Articles SET title = :title, content = :content, lastEdited = :lastEdited WHERE id = :id;");
            $sth->bindParam(':id', $id , PDO::PARAM_INT, 11);
            $sth->bindParam(':title', $title , PDO::PARAM_STR, 100);
            $sth->bindParam(':content', $content, PDO::PARAM_STR);
            $sth->bindParam(':lastEdited', $lastEdited);
            $sth->execute();
        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }

	public function setPathFeaturedImage($idArticle, $pathFeaturedImage)
	{
		try
		{
			$sth = $this->db->prepare("UPDATE Articles SET pathFeaturedImage = :pathFeaturedImage WHERE id = :idArticle;");
			$sth->bindParam(':pathFeaturedImage', $pathFeaturedImage , PDO::PARAM_STR, 100);
			$sth->bindParam(':idArticle', $idArticle , PDO::PARAM_INT, 11);
			$sth->execute();
		}
		catch (Exception $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
	}

	public function showArticlesfromMonthYear(int $month,int $year): array
	{
        $articles= array();
        try
        {
            $result = $this->db->prepare("SELECT * FROM Articles WHERE MONTH(timeCreated) = :month AND YEAR(timeCreated)= :year ORDER BY timeCreated DESC");
            $result->bindParam(':month', $month, PDO::PARAM_INT);
            $result->bindParam(':year', $year, PDO::PARAM_INT);
            $result->execute();

            while ($article = $result->fetchObject('Article')) {
                $articles[] = $article;
            }
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $articles;
    }

	public function getAllDates()
	{
		$dates = array();
		try
		{
			$result = $this->db->query("SELECT timeCreated FROM Articles ORDER BY timeCreated DESC");
			while ($article = $result->fetchObject('Article')) {
				$dates[] = $article->getTimeCreated();
			}
		} catch (Exception $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
		return $dates;
	}

    public function showArticlesByKeyword(String $keywords): array
    {
        $results = array();
        try {
            $sth = $this->db->prepare("SELECT * FROM Articles WHERE content LIKE CONCAT('%',:keywordsContent,'%') OR title LIKE CONCAT('%',:keywordsTitle,'%')");
            $sth->bindParam(':keywordsContent', $keywords);
            $sth->bindParam(':keywordsTitle', $keywords);
            $sth->execute();

            while ($result = $sth->fetchObject('Article')) {
                $results[] = $result;
            }
        } catch (Exception $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $results;
    }

	public function getAmountOfArticlesFromMonthAndYear(int $month,int $year): int
	{
		$counter = 0;
		try
		{
			$result = $this->db->prepare("SELECT * FROM Articles WHERE MONTH(timeCreated) = :month AND YEAR(timeCreated)= :year ORDER BY timeCreated DESC");
			$result->bindParam(':month', $month, PDO::PARAM_INT);
			$result->bindParam(':year', $year, PDO::PARAM_INT);
			$result->execute();

			while ($article = $result->fetchObject('Article')) {
				$counter = ($counter + 1);
			}
		} catch (Exception $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
		return $counter;
	}

}
