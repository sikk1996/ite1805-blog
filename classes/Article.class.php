<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 17.04.2018
 * Time: 15:46
 */

class Article
{
    private $id;
    private $idBlog;
    private $title;
    private $content;
    private $pathFeaturedImage;
    private $counter;
    private $lastEdited;
    private $timeCreated;
    private $commentCounter;
    private $author;

    function __construct(){}

    static function setAttributes($idBlog, $title, $content) {
        $instance = new self();
        $instance->setIdBlog($idBlog);
        $instance->setTitle($title);
        $instance->setContent($content);
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdBlog()
    {
        return $this->idBlog;
    }

    /**
     * @param mixed $idBlog
     */
    public function setIdBlog($idBlog): void
    {
        $this->idBlog = $idBlog;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getPathFeaturedImage()
    {
        return $this->pathFeaturedImage;
    }

    /**
     * @param mixed $pathFeaturedImage
     */
    public function setPathFeaturedImage($pathFeaturedImage): void
    {
        $this->pathFeaturedImage = $pathFeaturedImage;
    }

    /**
     * @return mixed
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @param mixed $counter
     */
    public function setCounter($counter): void
    {
        $this->counter = $counter;
    }

    /**
     * @return mixed
     */
    public function getLastEdited()
    {
        return $this->lastEdited;
    }

    /**
     * @param mixed $lastEdited
     */
    public function setLastEdited($lastEdited): void
    {
        $this->lastEdited = $lastEdited;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @param mixed $timeCreated
     */
    public function setTimeCreated($timeCreated): void
    {
        $this->timeCreated = $timeCreated;
    }

	/**
	 * @return mixed
	 */
	public function getCommentCounter()
	{
		return $this->commentCounter;
	}

	/**
	 * @param mixed $commentCounter
	 */
	public function setCommentCounter($commentCounter): void
	{
		$this->commentCounter = $commentCounter;
	}

	/**
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param mixed $author
	 */
	public function setAuthor($author): void
	{
		$this->author = $author;
	}



}