<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 17.04.2018
 * Time: 17:32
 */

class Blog_db
{
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function showAllBlogs(): array
    {
        $blogs = array();
        try
        {
            $result = $this->db->query("SELECT * FROM Blogs ORDER BY blogName");

            while ($blog = $result->fetchObject('Blog')) {
                $blogs[] = $blog;
            }
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $blogs;
    }

    public function getBlog(int $id)
    {
        $blog = null;
        try
        {
            $sth = $this->db->prepare("SELECT * FROM Blogs WHERE id = :id");
            $sth->bindParam(':id', $id, PDO::PARAM_INT, 11);
            $sth->execute();
            $blog = $sth->fetchObject("Blog");
        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $blog;
    }

    public function newBlog(Blog $blog): int
    {
        $idUser = $blog->getIdUser();
        $blogName = $blog->getBlogName();
        $about = $blog->getAbout();
        $idBlog=-1;
        try
        {
            $sth = $this->db->prepare("INSERT INTO Blogs (`blogName`, `idUser`, `about`) VALUES (:blogName, :idUser, :about);");
            $sth->bindParam(':blogName', $blogName , PDO::PARAM_STR, 45);
            $sth->bindParam(':idUser', $idUser, PDO::PARAM_INT, 11);
            $sth->bindParam(':about', $about, PDO::PARAM_STR, 65536);
            $sth->execute();
            $idBlog = $this->db->lastInsertId();
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $idBlog;
    }

    public function editBlog(Blog $blog)
    {
        $idUser = $blog->getIdUser();
        $blogName = $blog->getBlogName();
        $about = $blog->getAbout();
        $idBlog = $blog->getId();

        try
        {
            $sth = $this->db->prepare("UPDATE Blogs SET idUser = :idUser, blogName = :blogName, about = :about WHERE id = :idBlog;");
            $sth->bindParam(':idUser', $idUser , PDO::PARAM_INT, 11);
            $sth->bindParam(':idBlog', $idBlog , PDO::PARAM_INT, 11);
            $sth->bindParam(':blogName', $blogName , PDO::PARAM_STR, 45);
            $sth->bindParam(':about', $about, PDO::PARAM_STR,65536);
            $sth->execute();
        }
        catch (InvalidArgumentException $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }

    public function getBlogByUser(int $idUser)
    {
        $blog = null;
        try
        {
            $sth = $this->db->prepare("SELECT * FROM Blogs WHERE idUser = :id");
            $sth->bindParam(':id', $idUser, PDO::PARAM_INT, 11);
            $sth->execute();
            $blog = $sth->fetchObject("Blog");
        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $blog;
    }

	public function setBlogBannerImage(int $idBlog, string $pathBannerImage)
	{
		try
		{
			$sth = $this->db->prepare("UPDATE Blogs SET pathBannerImage = :pathBannerImage WHERE id = :idBlog;");
			$sth->bindParam(':pathBannerImage', $pathBannerImage , PDO::PARAM_STR, 100);
			$sth->bindParam(':idBlog', $idBlog , PDO::PARAM_INT, 11);
			$sth->execute();
		}
		catch (InvalidArgumentException $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
	}
}
