<?php
/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 17.04.2018
 * Time: 16:23
 */

class Comment
{
    private $id;
    private $idArticle;
    private $idUser;
    private $comment;
    private $timeCreated;

    function __construct(){}

    static function setAttributes($idArticle, $idUser, $comment) {
        $instance = new self();
        $instance->setIdArticle($idArticle);
        $instance->setIdUser($idUser);
        $instance->setComment($comment);
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * @param mixed $idArticle
     */
    public function setIdArticle($idArticle): void
    {
        $this->idArticle = $idArticle;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @param mixed $timeCreated
     */
    public function setTimeCreated($timeCreated): void
    {
        $this->timeCreated = $timeCreated;
    }
}