<?php
/**
 * Created by PhpStorm.
 * User: fredr
 * Date: 04/17/18
 * Time: 21:40
 */

class User_db
{
	private $db;

	public function __construct(PDO $db)
	{
		$this->db = $db;
	}

	public function createUser(User $user): int
	{
		$firstname = $user->getFirstname();
		$lastname = $user->getLastname();
		$email = $user->getEmail();
		$passwordHash = $user->getPasswordHash();
		$verificationKey = $user->getVerificationKey();

		$id = -1;

		try {
			$sth = $this->db->prepare("INSERT INTO Users (firstname, lastname, email, passwordHash, verificationKey) VALUES (:firstname, :lastname, :email, :passwordHash, :verificationKey);");
			$sth->bindParam(':firstname', $firstname, PDO::PARAM_STR, 45);
			$sth->bindParam(':lastname', $lastname, PDO::PARAM_STR, 45);
			$sth->bindParam(':email', $email, PDO::PARAM_STR, 45);
			$sth->bindParam(':passwordHash', $passwordHash, PDO::PARAM_STR, 45);
			$sth->bindParam(':verificationKey', $verificationKey, PDO::PARAM_STR, 36);
			if ($sth->execute() === true) {
				$id = $this->db->lastInsertId();
			}

		} catch (Exception $e) {
			print $e->getMessage() . PHP_EOL;
		}
		return $id;
	}

	public function getVerificationKey(int $id): string
	{
		$verificationKey = null;

		try {
			$sth = $this->db->prepare("SELECT verificationKey FROM Users WHERE id = :id;");
			$sth->bindParam(':id', $id, PDO::PARAM_INT, 11);
			if ($sth->execute() === true) {
				$verificationKey = $sth->fetch();
			}
		} catch (Exception $e) {
			print $e->getMessage() . PHP_EOL;
		}
		return $verificationKey;
	}

	public function setVerified(string $verificationKey, bool $verified)
	{
		try {
			$sth = $this->db->prepare("UPDATE Users SET verified = :verified WHERE verificationKey = :verificationKey;");
			$sth->bindParam(':verificationKey', $verificationKey, PDO::PARAM_STR, 36);
			$sth->bindParam(':verified', $verified, PDO::PARAM_BOOL);
			$sth->execute();
		} catch (Exception $e) {
			print $e->getMessage() . PHP_EOL;
		}
	}

	public function getVerified(string $email): bool
	{
		$verified = false;

		try {
			$sth = $this->db->prepare("SELECT verified FROM Users WHERE email = :email;");
			$sth->bindParam(':email', $email, PDO::PARAM_STR, 45);
			if ($sth->execute() === true) {
				$verified = $sth->fetchColumn();
			}
		} catch (Exception $e) {
			print $e->getMessage() . PHP_EOL;
		}
		return $verified;
	}

    public function getPassword_hash(String $email): string
    {
	    $passwordHash = "error";

        try {
            $sth = $this->db->prepare("SELECT passwordHash FROM Users WHERE email = :email;");
            $sth->bindParam(':email', $email, PDO::PARAM_STR, 45);
            if ($sth->execute() === true) {
                $passwordHash = $sth->fetchColumn();
                //var_dump($passwordHash);
            }
        } catch (Exception $e) {
            print $e->getMessage() . PHP_EOL;
        }
        return $passwordHash;
    }

    public function getUser(string $email) //: User
    {
    	$user = null;

	    try {
		    $sth = $this->db->prepare("SELECT * FROM Users WHERE email = :email;");
		    $sth->bindParam(':email', $email, PDO::PARAM_STR, 45);
		    if ($sth->execute() === true) {
			    $user = $sth->fetchObject('User');
		    }
	    } catch (Exception $e) {
		    //print $e->getMessage() . PHP_EOL;
	    }
	    return $user;
    }

	public function getUserById(int $id): User
	{
		$user = null;

		try {
			$sth = $this->db->prepare("SELECT * FROM Users WHERE id = :id;");
			$sth->bindParam(':id', $id, PDO::PARAM_INT, 11);
			if ($sth->execute() === true) {
				$user = $sth->fetchObject('User');
			}
		} catch (Exception $e) {
			print $e->getMessage() . PHP_EOL;
		}
		return $user;
	}

	public function editNamesOfUser(User $user)
	{
		$idUser = $user->getId();
		$firstname = $user->getFirstname();
		$lastname = $user->getLastname();

		try
		{
			$sth = $this->db->prepare("UPDATE Users SET firstname = :firstname, lastname = :lastname WHERE id = :idUser;");
			$sth->bindParam(':firstname', $firstname , PDO::PARAM_STR, 45);
			$sth->bindParam(':lastname', $lastname , PDO::PARAM_STR, 45);
			$sth->bindParam(':idUser', $idUser , PDO::PARAM_INT, 11);
			$sth->execute();
		}
		catch (Exception $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
	}

	public function setPathUserImage(int $idUser, string $pathUserImage)
	{
		try
		{
			$sth = $this->db->prepare("UPDATE Users SET pathUserImage = :pathUserImage WHERE id = :idUser;");
			$sth->bindParam(':pathUserImage', $pathUserImage , PDO::PARAM_STR, 100);
			$sth->bindParam(':idUser', $idUser , PDO::PARAM_INT, 11);
			$sth->execute();
		}
		catch (Exception $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
	}

	public function getAllUsers(): array
	{
		$users = array();
		try
		{
			$result = $this->db->query("SELECT * FROM Users");

			while ($user = $result->fetchObject('User')) {
				$users[] = $user;
			}
		} catch (Exception $e)
		{
			print $e->getMessage() . PHP_EOL;
		}
		return $users;
	}
    public function editPasswordHash(User $user)
    {
        $idUser = $user->getId();
        $passwordHash= $user->getPasswordHash();

        try
        {
            $sth = $this->db->prepare("UPDATE Users SET passwordHash = :passwordHash WHERE id = :idUser;");
            $sth->bindParam(':idUser', $idUser , PDO::PARAM_INT, 11);
            $sth->bindParam(':passwordHash', $passwordHash, PDO::PARAM_STR, 45);
            $sth->execute();
        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }
}