<?php
//Not done

/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 23.04.2018
 * Time: 14:46
 */

class Subcomment_db
{
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function showAllSubComments(int $idComment): array
    {
        $subcomments = array();
        try
        {
            $sth = $this->db->prepare("SELECT * FROM SubComments WHERE idComment = :idComment ORDER BY timeCreated ");
            $sth->bindParam(':idComment', $idComment, PDO::PARAM_INT, 11);
            $sth->execute();

            while ($subcomment = $sth->fetchObject('SubComment')) {
                $subcomments[] = $subcomment;
            }
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $subcomments;
    }

    public function newSubComment(int $idComment, int $idUser, string $comment)
    {
        try
        {
            $sth = $this->db->prepare("INSERT INTO SubComments (idComment, `idUser`, `comment`) VALUES (:idComment, :idUser, :comment);");
            $sth->bindParam(':idComment', $idComment , PDO::PARAM_INT, 11);
            $sth->bindParam(':idUser', $idUser, PDO::PARAM_INT, 11);
            $sth->bindParam(':comment', $comment, PDO::PARAM_STR, 65536);
            $sth->execute();
        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }

    public function getNumberOfSubComments(int $idComment): int
    {
        $numberOfSubComments = 0;
        try
        {
            $sth = $this->db->prepare("SELECT COUNT(id) FROM SubComments WHERE idComment = :idComment");
            $sth->bindParam(':idComment', $idComment , PDO::PARAM_INT, 11);
            $sth->execute();
            $numberOfSubComments = $sth->fetchColumn();

        }
        catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $numberOfSubComments;
    }
    public function deleteSubComment(int $idComment)
    {
        try
        {
            $sth = $this->db->prepare("DELETE FROM SubComments WHERE id = :id");
            $sth->bindParam(':id', $idComment, PDO::PARAM_INT, 11);
            $sth->execute();
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
    }
    public function showSubComments(int $id)
    {
        $subcomment = null;
        try
        {
            $sth = $this->db->prepare("SELECT * FROM SubComments WHERE id = :id");
            $sth->bindParam(':id',$id, PDO::PARAM_INT, 11);
            $sth->execute();

            $subcomment = $sth->fetchObject('SubComment');
        } catch (Exception $e)
        {
            print $e->getMessage() . PHP_EOL;
        }
        return $subcomment;
    }
}