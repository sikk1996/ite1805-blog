<?php
//Not done

/**
 * Created by PhpStorm.
 * User: lenetangstad
 * Date: 23.04.2018
 * Time: 14:43
 */

class Subcomment
{
    private $id;
    private $idComment;
    private $idUser;
    private $comment;
    private $timeCreated;

    function __construct(){}

    static function setAttributes($idComment, $idUser, $comment) {
        $instance = new self();
        $instance->setIdComment($idComment);
        $instance->setIdUser($idUser);
        $instance->setComment($comment);
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdComment()
    {
        return $this->idComment;
    }

    /**
     * @param mixed $idComment
     */
    public function setIdComment($idComment): void
    {
        $this->idComment = $idComment;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @param mixed $timeCreated
     */
    public function setTimeCreated($timeCreated): void
    {
        $this->timeCreated = $timeCreated;
    }
}