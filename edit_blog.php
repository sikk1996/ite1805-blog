<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19.04.2018
 * Time: 19:04
 */

require_once 'helper.php';

if (isset($_SESSION['loggedIn'])) {
    if ($_SESSION['verified']==false) {
        header("Location: login.php?verified=false");
    } else  {

        $blog = $blog_db->getBlogByUser($_SESSION['id']);
        if ($blog==null) {
            header("Location: new_blog.php");
        }

        if (isset($_POST['submit']) && !empty($_POST['submit'])) {
            $blogName = $_POST['blogName'];
            $about = $_POST['about'];

            $blog->setAbout($about);
            $blog->setBlogName($blogName);
            $blog_db->editBlog($blog);

            if (($_FILES['image']['size'] > 0)) {
                try {
                    $pathBannerImage = uploadImage($_FILES['image']);
                    $blog_db->setBlogBannerImage($blog->getId(), $pathBannerImage);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
            }

            if (isset($error)) { //If any error occurs
                echo $twig->render('templates/edit_blog.twig', array('session' => $_SESSION,'owner'=>true, 'message' => $e->getMessage()));
            }
            else { //When everything is ok and blog has been edited.
                header("Location: blog.php?id=". $blog->getId());
            }


        } else {
            echo $twig->render('templates/edit_blog.twig', array('session' => $_SESSION, 'owner'=>true,'blog'=> $blog));

        }

    }
} else {
    header("Location: login.php");
}



