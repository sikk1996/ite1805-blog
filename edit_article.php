<?php
/**
 * Created by PhpStorm.
 * User: fredr
 * Date: 05/01/18
 * Time: 18:28
 */

require_once 'helper.php';


if (isset($_SESSION['loggedIn'])) {
	if ($_SESSION['verified']) {
		if (isset($_GET['id'])) {

			$article = $article_db->showOne($_GET['id']);
			$blog = $blog_db->getBlog($article->getIdBlog());
			$blogTags = $tag_db->showTagsByBlog($blog->getId());
			$articleTags = $tag_db->showTagsByArticle($article->getId());

			if ($blog->getIdUser() != $_SESSION['id']) { //Sjekker om brukeren ikke eier bloggen
				header("Location: index.php?urNotTheOwner");
			}

			if (isset($_POST['submit']) && !empty($_POST['submit'])) {
				$updatedArticle = Article::setAttributes($blog->getId(), $_POST['title'], $_POST['content']);
				$updatedArticle->setId($article->getId());
				$article_db->edit($updatedArticle);

				try {
					if ($_FILES['image']['size'] > 0) {
						$pathFeaturedImage = uploadImage($_FILES['image']);
						$article_db->setPathFeaturedImage($article->getId(), $pathFeaturedImage);
					}
					if ($_FILES['files']['size'][0] > 0) {
						$files= reArrayFiles($_FILES['files']);
						if (isValidAttachments($files)) {

							foreach ($files as $attachment) {
								$tmp_attachment = new Attachment();
								$tmp_attachment->setIdArticle($article->getId());
								$tmp_attachment->setBlob(file_get_contents($attachment['tmp_name']));
								$tmp_attachment->setFilename($attachment['name']);
								$tmp_attachment->setMimeType($attachment['type']);
								$tmp_attachment->setSize($attachment['size']);

								if (is_uploaded_file($attachment['tmp_name'])) {
									$attachment_db->add($tmp_attachment);
								}
							}
						}
					}
					if(!empty($_POST['appliedTags'])) {
						$tag_db->clearAllTagsFromArticle($article->getId());
						foreach($_POST['appliedTags'] as $articleTagId) {
							$tag_db->applyToArticle($articleTagId, $article->getId());
						}
					}
					$articleTags = $tag_db->showTagsByArticle($article->getId());
					header("Location: article.php?id=".$article->getId());
					//echo $twig->render('templates/edit_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags, 'article' => $updatedArticle, 'articleTags' => $articleTags, 'message' => "Endringene er publisert"));
				} catch (Exception $e) {
					echo $twig->render('templates/edit_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags, 'article' => $updatedArticle, 'articleTags' => $articleTags, 'message' => $e->getMessage()));
				}

			} elseif (isset($_POST['tag-submit']) && !empty($_POST['tag-submit'])) {
				try {
					$tag = Tag::setAttributes($blog->getId(), strtolower($_POST['tag']));
					$tag_db->add($tag);
					header("Refresh:0");
				} catch (Exception $e) {
					echo $twig->render('templates/new_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags, 'message' => $e->getMessage()));
				}
			} else {
				echo $twig->render('templates/edit_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags, 'article' => $article, 'articleTags' => $articleTags));
			}
		} else {
			header("Location: ./");
		}
	} else {
		echo "you are not verified...";
	}
} else {
	header("Location: login.php");
}