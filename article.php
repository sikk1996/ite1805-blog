<?php

require_once 'helper.php';

$attachments = array();

if (isset($_GET['id'])) {
	$article = $article_db->showOne($_GET['id']);
	$blog = $blog_db->getBlog($article->getIdBlog());
	$user = $user_db->getUserById($blog->getIdUser());
	$attachments = $attachment_db->showAllByArticle($article->getId());
	$article->setAuthor($user->getFirstname() . " " . $user->getLastname());

    $allowNewComment = false;

	if (isset($_SESSION['loggedIn'])) {
	    $allowNewComment=true;
        if (isset($_POST['submit']) && !empty($_POST['submit'])) {
            if(isset($_GET['replyTo'])){
                $subComment_db->newSubComment($_GET['replyTo'],$_SESSION['id'],$_POST['comment']);
            } else {
                $comment_db->newComment($article->getId(),$_SESSION['id'],$_POST['comment']);
            }
        }
    }

    $comments = (array) $comment_db->showArticleComments($article->getId());
    $subComments = array();

        foreach ($comments as $comment) {
		$commentId = $comment->getId();
		$subComments = array_merge($subComments, $subComment_db->showAllSubComments($commentId));
	}
	$articleTags = $tag_db->showTagsByArticle($_GET['id']);

	if(isset($_GET['replyTo'])){
        $replyTo = $comment_db->showComment($_GET['replyTo']);
        echo $twig->render('templates/article.twig', array('session' => $_SESSION, 'article' => $article, 'blog' => $blog, 'comments' => $comments, 'subComments' => $subComments,'allowNewComment'=>$allowNewComment,'replyTo'=>$replyTo,'articleTags'=>$articleTags, 'attachments' => $attachments));
    } elseif (isset($_GET['idAttachment']) && is_numeric($_GET['idAttachment'])) {
		foreach ($attachments as $attachment) {
			if ($attachment instanceof Attachment) {
				if ($attachment->getId() == $_GET['idAttachment']) {
					showAttachment($attachment);
				}
			}
		}
	}else{
        echo $twig->render('templates/article.twig', array('session' => $_SESSION, 'article' => $article, 'blog' => $blog, 'comments' => $comments, 'subComments' => $subComments,'allowNewComment'=>$allowNewComment,'articleTags'=>$articleTags, 'attachments' => $attachments));

    }
} else {
    header("Location: index.php");

}


