<?php

require_once 'helper.php';

if (isset($_SESSION['loggedIn'])) {
	if ($_SESSION['verified'] == true) {

		$user = $user_db->getUserById($_SESSION['id']);

		if (isset($_POST['submit']) && !empty($_POST['submit'])) {
			$firstname = $_POST['firstname'];
			$lastname = $_POST['lastname'];

			$user->setFirstname($firstname);
			$user->setLastname($lastname);

			try {
				$user_db->editNamesOfUser($user);

				if (($_FILES['image']['size'] > 0)) {
					$pathUserImage = uploadImage($_FILES['image']);
					$user_db->setPathUserImage($user->getId(), $pathUserImage);
					$_SESSION['pathUserImage'] = $pathUserImage;
				}
				echo $twig->render('templates/edit_user.twig', array('session' => $_SESSION, 'user' => $user,
					'message' => "Endringene er lagret"));

			} catch (Exception $e) {
				echo $twig->render('templates/edit_user.twig', array('session' => $_SESSION, 'user' => $user,
					'message' => $e->getMessage()));
			}
		} else if(isset($_POST['submitPassword'])){
		    if($_POST['newPassword1']==$_POST['newPassword2']) {
                if(password_verify($_POST['oldPassword'],$user->getPasswordHash())){
                    $user->setPasswordHash(password_hash($_POST['newPassword1'],PASSWORD_DEFAULT));
                    $user_db->editPasswordHash($user);
                    echo $twig->render('templates/edit_user.twig', array('session' => $_SESSION, 'user' => $user,
                        'message' => "Password succesfully changed"));
                } else {
                    echo $twig->render('templates/edit_user.twig', array('session' => $_SESSION, 'user' => $user,
                        'message' => "Feil passord"));
                }
            } else {
                echo $twig->render('templates/edit_user.twig', array('session' => $_SESSION, 'user' => $user,
                    'message' => "Passordene er ikke like"));
            }
        } else {
			echo $twig->render('templates/edit_user.twig', array('session' => $_SESSION, 'user' => $user));
		}
	} else {
		header("Location: login.php?verified=false");
	}
} else {
	header("Location: login.php");
}