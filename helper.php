<?php
setlocale (LC_TIME, "norwegian");
date_default_timezone_set("Europe/Oslo");

require_once 'vendor/autoload.php';
require_once 'db/auth_pdo.php';
require_once 'functions.php';

require_once 'classes/Article.class.php';
require_once 'classes/Attachment.class.php';
require_once 'classes/Blog.class.php';
require_once 'classes/Comment.class.php';
require_once 'classes/SubComment.class.php';
require_once 'classes/Tag.class.php';
require_once 'classes/User.class.php';

require_once 'classes/Article_db.class.php';
require_once 'classes/Attachment_db.class.php';
require_once 'classes/Blog_db.class.php';
require_once 'classes/Comment_db.class.php';
require_once 'classes/SubComment_db.class.php';
require_once 'classes/Tag_db.class.php';
require_once 'classes/User_db.class.php';

$article_db = new Article_db($db);
$attachment_db = new Attachment_db($db);
$blog_db = new Blog_db($db);
$comment_db = new Comment_db($db);
$subComment_db = new subComment_db($db);
$tag_db = new Tag_db($db);
$user_db = new User_db($db);

$loader = new Twig_Loader_Filesystem('/');
$twig = new Twig_Environment($loader);

session_start();

$users = $user_db->getAllUsers();
$twig->addGlobal('users', $users);
$blogs = $blog_db->showAllBlogs();
$twig->addGlobal('blogs', $blogs);
$allTags = $tag_db->showAllTags();
$twig->addGlobal('allTags',$allTags);


$dates = $article_db->getAllDates();
$monthsAndYears = array();

for ($i = 0, $l = count($dates); $i < $l; ++$i) {
	// If month and year doesn't exist in array at the same time.
	if (!(in_array_field(date('n', strtotime($dates[$i])), 'month', $monthsAndYears) &&
		in_array_field(date('Y', strtotime($dates[$i])), 'year', $monthsAndYears))) {
		$monthsAndYears[$i]['month'] = date('n', strtotime($dates[$i]));
		$monthsAndYears[$i]['year'] = date('Y', strtotime($dates[$i]));
		$monthsAndYears[$i]['counter'] = $article_db->getAmountOfArticlesFromMonthAndYear($monthsAndYears[$i]['month'], $monthsAndYears[$i]['year']);
	}
}

$twig->addGlobal('monthsAndYears',$monthsAndYears);
