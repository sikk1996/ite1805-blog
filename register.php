<?php

require_once 'helper.php';

if (isset($_POST['submit']) && !empty($_POST['submit'])) {
	$firstname = strip_tags($_POST['firstname']);
	$lastname = strip_tags($_POST['lastname']);
	$email = strip_tags($_POST['email']);
	$password = strip_tags($_POST['password']);
	$passwordHash = password_hash($password, PASSWORD_DEFAULT);

	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	$genKey = md5(uniqid(rand(), 1));
	curl_setopt($ch, CURLOPT_URL, "http://kark.uit.no/internett/php/mailer/mailer.php?address=" . $email . "&url=https://kark.uit.no/~fbe025/ITE1805_blog_develop/verify.php?verificationKey=" . $genKey);
	$user = User::setAttributes($firstname, $lastname, $email, $passwordHash, $genKey);
	if ($user_db->createUser($user) != -1) {
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);

		if (!curl_errno($ch)) {
			echo $twig->render('templates/register.twig', array('success' => true));
		}
	}
	curl_close($ch);
} else {
	echo $twig->render('templates/register.twig');
}