<?php

require_once 'helper.php';
$commentId = $_GET['id'];

if (isset($_SESSION['loggedIn'])) {
    if ($_SESSION['verified']) {
        $comment = $comment_db->showComment($commentId);
        $blog = $blog_db->getBlogByUser($_SESSION['id']);
        $article = $article_db->showOne($comment->getIdArticle());

        if ($comment->getIdUser() == $_SESSION['id'] || $article->getIdBlog() == $blog->getId()) { //Sjekker om brukeren eier kommentar eller om brukerer bloggeier
            $comment_db->deleteComment($commentId);
            header("Location: article.php?id=".$article->getId());
        } else {
            header("Location: index.php?urNotTheOwner");

        }
    } else {
        echo "you are not verified...";
    }
} else {
    header("Location: login.php");
}


