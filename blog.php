<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 24.04.2018
 * Time: 12:11
 */

require_once 'helper.php';
if (isset($_GET['id'])) {
    $blog = $blog_db->getBlog($_GET['id']);
    $articles = (array) $article_db->showAlByBlog($_GET['id']);
} else {
    header("Location: ./");

}

foreach ($articles as $article) {
	$articleId = $article->getId();
	$numberOfComments = $comment_db->getNumberOfComments($articleId);
	$comments = (array) $comment_db->showArticleComments($articleId);

	foreach ($comments as $comment) {
		$idComment = $comment->getId();
		$numberOfSubComments = $subComment_db->getNumberOfSubComments($idComment);
		$numberOfComments += $numberOfSubComments;
	}
	$article->setCommentCounter($numberOfComments);
}

echo $twig->render('templates/blog.twig', array('session' => $_SESSION, 'articles' => $articles, 'blog' => $blog));