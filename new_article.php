<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19.04.2018
 * Time: 20:06
 */

require_once 'helper.php';

if (isset($_SESSION['loggedIn'])) {
	if ($_SESSION['verified']) {

		$blog = $blog_db->getBlogByUser($_SESSION['id']);
		$blogTags = $tag_db->showTagsByBlog($blog->getId());

		if ($blog->getIdUser() != $_SESSION['id']) { //Sjekker om brukeren ikke eier bloggen
			header("Location: index.php?urNotTheOwner");
		}

		if (isset($_POST['submit']) && !empty($_POST['submit'])) {
			$article = Article::setAttributes($blog->getId(), $_POST['title'], $_POST['content']);
			$idArticle = $article_db->add($article); // TODO: DETTE MÅ GJØRES OM PÅ... ARTIKKELEN PUBLISERES SELV OM DET SKJER FEIL MED FIL/BILDE-OPPLASTNINGEN!!!

			try {
				if ($_FILES['image']['size'] > 0) {
					$pathFeaturedImage = uploadImage($_FILES['image']);
					$article_db->setPathFeaturedImage($idArticle, $pathFeaturedImage);
				}
				if ($_FILES['files']['size'][0] > 0) {
					$files= reArrayFiles($_FILES['files']);
					if (isValidAttachments($files)) {

						foreach ($files as $attachment) {
							$tmp_attachment = new Attachment();
							$tmp_attachment->setIdArticle($idArticle);
							$tmp_attachment->setBlob(file_get_contents($attachment['tmp_name']));
							$tmp_attachment->setFilename($attachment['name']);
							$tmp_attachment->setMimeType($attachment['type']);
							$tmp_attachment->setSize($attachment['size']);

							if (is_uploaded_file($attachment['tmp_name'])) {
								$attachment_db->add($tmp_attachment);
							}
						}
					}
				}
				if(!empty($_POST['appliedTags'])) {
					foreach($_POST['appliedTags'] as $articleTagId) {
						$tag_db->applyToArticle($articleTagId, $idArticle);
					}
				}
				header("Location: article.php?id=".$idArticle);
				//echo $twig->render('templates/new_article.twig', array('session' => $_SESSION,'blogTags' => $blogTags, 'message' => "Artikkelen er publisert"));
			} catch (Exception $e) {
				echo $twig->render('templates/new_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags, 'message' => $e->getMessage()));
			}

		} elseif (isset($_POST['tag-submit']) && !empty($_POST['tag-submit'])) {
			try {
				$tag = Tag::setAttributes($blog->getId(), strtolower($_POST['tag']));
				$tag_db->add($tag);
				header("Refresh:0");
			} catch (Exception $e) {
				echo $twig->render('templates/new_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags, 'message' => $e->getMessage()));
			}
		} else {
			echo $twig->render('templates/new_article.twig', array('session' => $_SESSION, 'blogTags' => $blogTags));
		}
	} else {
		echo "you are not verified...";
	}
} else {
	header("Location: login.php");
}


