ITE1805
==================
Prosjektoppgave - Gruppe 5
==================


Oppgaven går ut på å utvikle en blogg. Løsningen skal baseres på bruk av HTML 5, CSS, PHP og evt. klient JavaScript. Bloggen skal benytte en database for lagring av informasjon. MySQL (MariaDb) på kark.hin.no benyttes til dette. Det er ønskelig at bloggen har følgende funksjonalitet:

**Krav til bloggfunksjonalitet:**

1. Løsningen skal ha støtte for mange  blogger. En registrert bruker skal kunne opprette sin egen blogg og bli eier av denne.
2. Hovedsiden for en blog bør vise alle, evt. et utvalg av alle blogginnlegg sortert på dato med antall kommentarer.
3. Eieren av bloggen skal måtte logge seg på for å legge inn nye innlegg.
4. Anonyme brukere skal kunne lese innlegg.
5. Brukere må kunne registrere seg med nødvendig informasjon. Passord for brukere bør lagres med bruk av hash i databasen. (Benytte php metodene password_hash() og password_verify() ). Du bestemmer selv annen relevant informasjon som bør lagres for en bruker.
6. Ved registrering bør det sendes epost til brukeren for bekreftelse av epostadressen, bekreftelse er nødvendig før konto tas i bruk. Notatet Sending av epost for prosjektoppgaven inneholder beskrivelse av hvordan sende epost.
7. Det bør være mulig å kommentere innlegg og kommentarer. Kun registrerte bruker skal kunne gjøre dette.
8. Du bestemmer selv formatet for et blogg innlegg, hvilke elementer som skal være med. Naturlige elementer vil være: Tittel, tekst for innlegget, dato, forfatter, stikkord.
9. Et blogginnlegg bør kunne ha vedlegg av begrenset størrelse. Dette kan for eksempel være bilder. Vedlegg lagres i databasen og vises ved lesing av innlegget. Typen vedlegg og størrelsen på disse bør begrenses.
10. Et blogginnlegg bør kunne ha en eller flere tags (stikkord). Det bør være mulig å hente ut alle innlegg basert på tag, nye tags bør kunne opprettes.
11. Eldre innlegg bør kunne aksesseres på månedsbasis, slik at det er lett å få opp innlegg for en aktuell måned, oversikten bør inneholde antall innlegg denne måneden.
12. Det kan være trefftellere på de ulike innleggene som inkrementeres ved aksess og vises på web.
13. Det bør være mulig å søke på innleggenes emne og innhold for å finne det en er på jakt etter.
14. Eieren av bloggen bør kunne drive vedlikehold av bloggen og foreta operasjoner som sletting av upassende kommentarer.
15. Sletting av kommentarer skal logges i egen loggtabell som viser den slettede kommentaren samt tidspunkt (dato og klokkeslett) for slettingen, bruk med fordel en trigger her.

**Krav til database.**

Databasen blir fundamentet i bloggen og må være normalisert opp til 3NF. Det anbefales å bruke WorkBench til modellering og konstruksjon av databasen. Databasen må modelleres med relasjoner, primærnøkler og fremmednøkler etter prinsipper for ER-modellering. Databasen må ha støtte for all funksjonalitet nevnt under "Krav til bloggfunksjonalitet". Det vil si at datamodellen kan benyttes for å lage en komplett løsning som spesifisert, som så via PHP-kode benytter seg av den forrigling og funksjonalitet databasen gjennom dette tilbyr. Datamodellen er en del av vurderingsgrunnlaget.

**Generelle krav til løsningen.**

Løsningen bør fungere i de mest vanlige nettleserne på ulike plattformer.
Alle sider bør benytte samme stilark slik at stil/utseende blir ensartet. JavaScript kan benyttes ved behov
Løsningen må ha beskyttelse mot SQL injection og HTML/JavaScript i innlegg og kommentarer.
HTML og CSS koden skal være i henhold til W3C's anbefalinger.
Benytt objektorienterte prinsipper ved PHP kodingen.
Sesjonsdata bør beskyttes.
I MySQL skal tabelltypen være INNODB med støtte for fremmednøkler, referanseintegritet skal ivaretas.
HTML og PHP koden bør adskilles ved bruk av et template system ala Twig.

**NB!** All kode som benyttes skal være egenprodusert. I de tilfellene der det benyttes kode skrevet av andre skal dette tydelig opplyses med referanse. Kildehenvisninger er meget viktig. Hver student må fylle ut egenærklæring angående egen kode og bruk av kilder. Egenerklæringen er obligatorisk og ligger her: Egenerklæring prosjektoppgaven

**GIT**

All kode skal under hele prosjektperioden ligge på GITLab løsningen hos UIT, adresse https://source.uit.no/ (Lenker til en ekstern side.)Lenker til en ekstern side.. GIT skal benyttes aktivt av alle prosjektmedlemmer i utviklingen slik at historikken vil vise hvem som har gjort hva og når til en hver tid. Faglærere må gis tilgang til repository fra starten av prosjektet, ast175@uit.no og kco006@uit.no må gis Reporter access til prosjektet.

**Prosjektdagbok.**

For å dokumentere arbeidsinnsatsen til gruppens medlemmer er det viktig at det benyttes prosjektdagbok i hele prosjektets gang. Prosjektdagboken skal inneholde beskrivelse av arbeidsoppgaver som er utførte og tidsforbruk for disse, her skal det føres antall timer pr aktivitet og summen av timer for hele prosjektet pr. deltaker. GIT loggen vil her være til stor hjelp. Prosjektdagboken vil, sammen med eventuelle andre dokumenter (kildekode o.l.),  være dokumentasjon på utført arbeide. Hvert gruppemedlem skal føre sin egen prosjektdagbok.

**Prosjektmøte.**

Hver gruppe vil innkalles til status møte med faglærer. På dette møtet er det forventet at gruppen framlegger utkast til databasemodell og beskriver planlagt løsning. Møtene vil finne sted i uke 15.

**Prosjekt presentasjon:**

Hver gruppe skal foreta en presentasjon av sin løsningen. Presentasjonen er obligatorisk og vil være det avsluttende møtet mellom faglærer og gruppen hvor prosjektet skal gjennomgås. Tidspunkt og plan for presentasjoner vil bli presentert senere. 

**Innlevering**

Prosjektrapport og kildekode.
Sammen med kildekode skal også SQL-script  brukt for opprettelse av databasetabeller, triggere og annet som inngår i løsningen leveres. Dette kan du gjøre med "reveres engineering" eller "forward engineering" fra WorkBench. Data i tabeller er ikke nødvendig for innleveringens skyld. Også lever / sett inn i rapporten, en skjermdump av komplett ER-databasemodell.

Det skal leveres en prosjektrapport som minimum omfatter følgende elementer:

* Navn på alle deltakerne og gruppenummer/navn.
* Nødvendige nettadresser (URL) og brukernavn og passord for å kunne vurdere alle aspekter ved løsningen. Typisk både brukernavn og passord for administrator/eier og vanlig bruker må oppgis.
* Beskrivelse av valgt løsning og hvorfor man har valgt denne løsningen inklusive oversikt over filer/klasser, dokumenter, databasemodell og liknende som inngår i løsningen
* Kommentarer til oppgaven slik den er gitt
* Kommentarer/evaluering knyttet til egen løsning
* Evaluering av hva man har lært av oppgaven
* Dokumentasjon av arbeidsinnsats pr. deltaker; hvem har gjort hva, og hvor mye tid er brukt. Hver deltaker leverer sin prosjektdagbok.
* Andre forutsetninger vedrørende oppgaven
* Prosjektrapporten (PDF format) og kildekode leveres elektronisk på Canvas som en ZIP fil. Løsningen må være kjørbar på webtjener, slik at det er lett for faglærer/sensor å vurdere løsningen. Det er derfor særdeles viktig at URL adresser og nødvendige brukernavn og passord er lett tilgjengelige, både for administrator og vanlig bruker. ZIP filen skal inneholde en fil med navn README.TXT der alle nødvendige URL adresser og brukernavn og passord ligger, denne informasjonen bør også være lett tilgjengelig i rapporten.
